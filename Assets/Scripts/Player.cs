﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {


    public Transform playerSpawnPoints;
    public bool respawn;
    Transform[] spawnPoints;
    public GameObject landingArea;
    bool lastToggle;
	// Use this for initialization
	void Start () {
        spawnPoints = playerSpawnPoints.GetComponentsInChildren<Transform>();
   


    }
	
	// Update is called once per frame
	void Update () {
	    if(lastToggle != respawn)
	    {
	        Respawn();
            respawn = false;
        }
        else
	    {
            lastToggle = respawn;
	    }
	}

    void Respawn()
    {
        int i = Random.Range(1, spawnPoints.Length);
        transform.position = spawnPoints[i].transform.position;
    }

    void OnFindClearArea()
    {
        Invoke("DropFlare",3f);
    }

    void DropFlare()
    {
        Instantiate(landingArea, transform.position, transform.rotation);
    }
}
