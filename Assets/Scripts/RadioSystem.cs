﻿using UnityEngine;
using System.Collections;

public class RadioSystem : MonoBehaviour {

   public AudioClip initialHeliCall, initialCallReply;
    AudioSource audioSource;
	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMakeInitialHelliCall()
    {
        print("boom ");
        audioSource.clip = initialHeliCall;
        audioSource.Play();
        Invoke("InitialReply",initialHeliCall.length + 1f);
    }

    void InitialReply()
    {
        audioSource.clip = initialCallReply;
        audioSource.Play();
        BroadcastMessage("OnDispatchHellicopter");
    }
}
