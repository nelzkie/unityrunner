﻿using UnityEngine;
using System.Collections;

public class InnverVoice : MonoBehaviour {

    public AudioClip whatHappened;
    public AudioClip goodLandingArea;

    AudioSource audioSource;
	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();

        audioSource.clip = whatHappened;
        audioSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnFindClearArea()
    {
        print("are you getting called");
        audioSource.clip = goodLandingArea;
        audioSource.Play();
        
        Invoke("CallHelli",goodLandingArea.length + 1f);
    }

    void CallHelli()
    {
        SendMessageUpwards("OnMakeInitialHelliCall");
    }

}
