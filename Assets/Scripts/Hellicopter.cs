﻿using UnityEngine;
using System.Collections;

public class Hellicopter : MonoBehaviour {

    bool called;
    AudioSource audioSource;

    private Rigidbody rigidbody;
    // Use this for initialization
    void Start () {
        audioSource = gameObject.GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
	}


	// Update is called once per frame
	void Update () {
  
	}

    // will be called on radiosystem class via braodcastMessage()
    public void OnDispatchHellicopter()
    {
        if (!called)
        {
            called = true;

            rigidbody.velocity = new Vector3(0,0,50f);
        }
    }
}
