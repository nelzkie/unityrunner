﻿using UnityEngine;
using System.Collections;

public class ClearArea : MonoBehaviour {
    public float timeSinceLastTrigger = 0;

    bool foundClearArea;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        timeSinceLastTrigger += Time.deltaTime;
        if(timeSinceLastTrigger > 1f && !foundClearArea)
        {
            print("foundClearArea");
           // https://docs.unity3d.com/ScriptReference/Component.SendMessageUpwards.html
            SendMessageUpwards("OnFindClearArea");
            foundClearArea = true;
        }
	}

    void OnTriggerStay(Collider collider)
    {
        timeSinceLastTrigger = 0f;
        
    }
}
